HLOG is a logistics solutions provider based in Cameroon, Africa. We are a team of individuals ready to assist you with commercial logistics, business transport, and individual moving and relocation assistance. Since 1972, we�ve provided logistics services to customers in various sectors.

Address: BP 35071, Yaound� 12196, Cameroon�

Phone: +237 22 21 86 35
